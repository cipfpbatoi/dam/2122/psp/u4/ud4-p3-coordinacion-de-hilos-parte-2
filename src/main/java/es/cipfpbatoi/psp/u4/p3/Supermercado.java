package es.cipfpbatoi.psp.u4.p3;

import java.util.concurrent.atomic.AtomicInteger;

public class Supermercado {

    private AtomicInteger ventasTotalesSupermercado = new AtomicInteger();
    private AtomicInteger tiempoDeEspera = new AtomicInteger();

    public int getVentasTotalesSupermercado() {
        return ventasTotalesSupermercado.get();
    }

    public void incrementarVentasTotalesSupermercado(int incremento) {
        ventasTotalesSupermercado.addAndGet(incremento);
    }

    public int getTiempoDeEspera() {
        return tiempoDeEspera.get();
    }

    public void incrementarTiempoDeEspera(int tiempoDeEspera) {
        this.tiempoDeEspera.addAndGet(tiempoDeEspera);
    }



}
