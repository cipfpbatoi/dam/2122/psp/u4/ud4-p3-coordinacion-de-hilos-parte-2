package es.cipfpbatoi.psp.u4.p3;

import java.util.concurrent.ThreadLocalRandom;

public class Cliente implements Runnable{

    Caja[] cajas = null;
    private static final int TIEMPO_COMPRANDO_MIN_MS = 500;
    private static final int TIEMPO_COMPRANDO_MAX_MS = 2000;

    public Cliente(Caja[] cajas) {

        this.cajas = cajas;

    }

    @Override
    public void run() {

        long tiempo_comprando = ThreadLocalRandom.current().nextInt(TIEMPO_COMPRANDO_MIN_MS,TIEMPO_COMPRANDO_MAX_MS);

        try {
            Thread.sleep(tiempo_comprando);
            int caja = ThreadLocalRandom.current().nextInt(0,cajas.length);
            System.out.println(Thread.currentThread().getName() + " Va a la caja " + caja);

            cajas[caja].checkout();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }

    }
}
