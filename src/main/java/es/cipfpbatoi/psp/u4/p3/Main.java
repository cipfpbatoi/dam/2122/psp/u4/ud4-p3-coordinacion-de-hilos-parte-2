package es.cipfpbatoi.psp.u4.p3;

public class Main {

    public static final int M = 15; //Clientes
    public static final int N = 5; //Cajas

    public static void main(String[] args) {

        //Caja
        Caja[] cajas = new Caja[N];

        //Cliente
        Cliente[] clientes = new Cliente[M];
        Thread[] clientesth = new Thread[M];

        Supermercado supermercado = new Supermercado();
        //Inicializamos las cajas
        for (int i = 0; i<N;i++){
            cajas[i] = new Caja(i,supermercado);
        }

        //Inicializamos los clientes
        for (int i = 0; i<M;i++){
            clientes[i] = new Cliente(cajas);
            clientesth[i] = new Thread(clientes[i],"Cliente " + i);
            clientesth[i].start();
        }

        for ( Thread clienteth : clientesth){
            try {
                clienteth.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        // El supermercado va a cerrar.

        System.out.println("======== Supermercado Cerrado ========");
        for (int i = 0; i<N;i++){
            System.out.println("Resumen caja: " + i + ": " + cajas[i].imprimirResumenCaja() );
        }
        System.out.println("TOTAL FACTURADO: "+ supermercado.getVentasTotalesSupermercado() + " €");
        System.out.println("TIEMPO MEDIO ESPERA: "+ supermercado.getTiempoDeEspera() / M +" s");

    }

}
