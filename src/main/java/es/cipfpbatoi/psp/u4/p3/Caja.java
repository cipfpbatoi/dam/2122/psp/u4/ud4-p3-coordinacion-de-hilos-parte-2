package es.cipfpbatoi.psp.u4.p3;

import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

public class Caja {

    private int importeTotalFacturado;
    private int tiempoEsperaTotal;
    private int clientesAtendidos;


    private AtomicInteger numeroClientesEnCola;

    private static final int IMPORTE_MIN_COBRADO_EUR = 5;
    private static final int IMPORTE_MAX_COBRADO_EUR = 100;

    private static final int TIEMPO_MIN_COBRO_MS = 1000;
    private static final int TIEMPO_MAX_COBRO_MS = 3000;

    private Object lockCaja = new Object();

    int clientesEnCola = 0;

    private Supermercado supermercado;

    int numCaja;

    public Caja(int numCaja,Supermercado supermercado){
        importeTotalFacturado = 0;
        tiempoEsperaTotal = 0;
        clientesAtendidos = 0;
        numeroClientesEnCola = new AtomicInteger(0);
        this.supermercado = supermercado;
        this.numCaja = numCaja;
    }

    public void checkout () throws InterruptedException
    {
        long horaLlegadaMS = System.currentTimeMillis();

        numeroClientesEnCola.getAndIncrement();

        synchronized (this) {

            numeroClientesEnCola.getAndDecrement();

            int tiempoAtencionMS = ThreadLocalRandom.current().nextInt(TIEMPO_MIN_COBRO_MS, TIEMPO_MAX_COBRO_MS);
            Thread.sleep(tiempoAtencionMS);
            int totalCobrado = ThreadLocalRandom.current().nextInt(IMPORTE_MIN_COBRADO_EUR, IMPORTE_MAX_COBRADO_EUR);
            int tiempoEsperandoSegundos = (int) (System.currentTimeMillis() - horaLlegadaMS - tiempoAtencionMS) / 1000;

            System.out.println("La Caja: " + numCaja + " ha atentido al: " + Thread.currentThread().getName() );
            System.out.println("\tCobrado: " + totalCobrado + " €");
            System.out.println("\tTiempo de Atención: " + tiempoAtencionMS / 1000 + " s");
            System.out.println("\tTiempo de Espera: " + tiempoEsperandoSegundos + " s");
            System.out.println("\tClientes en Cola: " + numeroClientesEnCola + " personas");

            importeTotalFacturado+=totalCobrado;
            supermercado.incrementarVentasTotalesSupermercado(totalCobrado);
            tiempoEsperaTotal+=tiempoEsperandoSegundos;
            supermercado.incrementarTiempoDeEspera(tiempoEsperandoSegundos);
            clientesAtendidos++;

        }
    }

    public int getTiempoEsperaTotal() {
        return tiempoEsperaTotal;
    }

    public String imprimirResumenCaja()
    {
        return " Clientes atendidos: "+clientesAtendidos+
                ", Total Facturado: "+importeTotalFacturado + " €";
    }


}
