# U4 - PRÁCTICA 2  - PROGRAMACIÓN MULTITHREADING - SEGUNDA PARTE

Coordinación de Hilos ( sincronizar, `wait`/`notify`/`notifyAll`)

Opcionalmente, Executors, colas bloqueantes.

Escribe en Java los programas necesarios:

* Utiliza de manera adecuada la Programación Orientada a Objetos.
* Presta atención a las excepciones que pueda originar el código e informa al usuario.
* Imprime en pantalla los mensajes necesarios cuando haya que informar al usuario.
* No olvides comentar el código y documentar los métodos.

## Ejercicio 5 - Supermercado

Escribe una aplicación multihilo que simule la compra real en un supermercado con M compradores y N cajas automáticas pasar la compra ( para pagar por la compra).

![Supermarket](md_media/supermarket-stalls.jpeg)

Descripción:

* Hay M clientes que entran en el supermercado y realizan la compra durante un periodo aleatorio de compra.
* Cuando un cliente finaliza la compra elije de manera aleatoria una de las máquinas y se pone a la espera hasta que se su turno de pagar.
* El proceso de pago una vez le ha tocado el turno al cliente es aleatorio y el importe también. Una vez el cliente ha pagado, el resultado se acumula en la variable `VentasTotalesDelSupermecado`.
* El supermercado cierra cuando todos los clientes se van, entonces imprime:
  * Por cada **caja automática** el importe facturado por esa caja y el número de clientes atendido.
  * El valor de la variable `VentasTotalesDelSupermecado` y el tiempo medio de espera para pagar.

Consideraciones:

* `M` y `N` son constantes para el supermecado.
* Cuando un cliente está pagando en una caja, otros clientes pueden elegir en esa caja para pagar y ponerse a la cola. ( Se debe gestionar el lock adecuadamente de forma que se libere cuando un cliente se está atendiendo un cliente).
* Cuando un cliente finaliza el proceso de compra:
  * Imprime el total que el cliente ha utilizado (tiempo de espera + tiempo siendo atendido)
  * Imprime también los clientes que están esperando.
* Escribe otros mensajes para mostrar que está ocurriendo en el supermercado.

## Ejercicio 6 - Supermercado Moderno (Opcional)

Implementa una clase SuperMercadoModerno con N cajas pera simular el ejemplo anterior, pero cuando un cliente finaliza la compra, espera a pagar en una única cola. Cuando una caja se vacía, el primero que esté a la espera irá a esa caja y pagará por la compra.

![Supermarket Single Queue Example](md_media/supermarketsinglequeue.png)

Calcula esl tiempo medio de espera ( y compáralo con el ejercicio anterior ). ¿Cuál es el mejor? ¿Cual tiene la implementación más eficiente? ¿Cual utilizarías tú si tuvieras que montar un supermercado?
